*** Setting ***
Resource          ../resources/init.robot

*** Variables ***
${Homepage}    https://insurance.tescolotusmoney.com/
${Browser}    chrome
${xpath_car_type}    //*[@id="car_type_selecter_desktop"]/div/button
${xpath_select_car_type}    //*[@id="app"]/div[1]/div/div/div[2]/div/div[1]/div[2]/div/div/div[1]/div/div
${xpath_select_car_company}    //*[@id="app"]/div[1]/div/div/div[2]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div  
${xpath_select_car_model}    //*[@id="app"]/div[1]/div/div/div[2]/div/div[3]/div[2]/div/div/div[1]/div/div 
${xpath_select_car_sub_model}    //*[@id="app"]/div[1]/div/div/div[2]/div/div[4]/div[2]/div/div/div[1]/div/div   
${xpath_select_car_year}    //*[@id="app"]/div[1]/div/div/div[2]/div/div[5]/div[2]/div/div/div[1]/div/div
${xpath_select_car_insurance_class}    //*[@id="modalTitleDesktop"]/div/div[1]/div/div[1]/div/div

*** Test case ***
TC_001 Filter data successful
    Given Open Browser    ${Homepage}    ${Browser}
    When Filter data all filed
    Then Close All Browsers


*** Keywords ***
Filter data all filed
    Sleep    5s
    Click Button    ${xpath_car_type}
    Sleep    2s
    Click Element    ${xpath_select_car_type} 
    Sleep    2s
    Click Element    ${xpath_select_car_company}
    Sleep    2s
    Click Element    ${xpath_select_car_model}
    Sleep    2s
    Click Element    ${xpath_select_car_sub_model}   
    Sleep    2s
    Click Element    ${xpath_select_car_year}
    Sleep    2s
    Click Element    ${xpath_select_car_insurance_class}
    Sleep    2s
    Close Window
    Sleep    5s
