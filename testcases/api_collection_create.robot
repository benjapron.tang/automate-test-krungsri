*** Settings ***
Resource          ../resources/init.robot

*** Test Cases ***
TC_CREATE_SUCCESS_01 - Create callection success
    [Documentation]    To ensure that the API is able to create callection success
    Given Common - Prepare Json Request    ${api_collection_create}
    When Call create colection    ${request}
    Then Common - Http response should be '200'
    And Common - Response Field 'collection.name' Should Be Equal 'Sample Collection'

