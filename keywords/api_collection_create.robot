*** Settings ***
Resource          ../resources/init.robot

*** Keywords ***
Call create colection
    [Arguments]    ${jsonbody}
    Create Session    post_create_collection    https://api.getpostman.com    disable_warnings=${1}
    &{headers}=    Create Dictionary
    ...    x-api-key=PMAK-6061d0092890bd004d697dc6-43deca25833ad6cd409eaba5bc1dd2f526
    ...    Content-Type=application/json
    ${response}=    Post Request    post_create_collection    /collections    data=${jsonbody}    headers=${headers}
    Run Keyword If    ${response.status_code} == 200    Set Test Variable    ${json_resp}    ${response.json()}
    Set Test Variable    ${response}    ${response}
    Log    ${response.text}

Call create promotion API with cms
    [Arguments]    ${jsonbody}
    Sign in    ${qa_user}    ${qa_password}
    Create Session    post_create_promotion    ${eggcrm_plus_api_url}    disable_warnings=${1}
    &{headers}=    Create Dictionary
    ...    Authorization=${authorization_key}
    ...    Content-Type=application/json
    ${response}=    Post Request    post_create_promotion    ${create_promotion_cms_uri}    data=${jsonbody}    headers=${headers}
    Run Keyword If    ${response.status_code} == 200    Set Test Variable    ${json_resp}    ${response.json()}
    Set Test Variable    ${response}    ${response}
    Log    ${response.text}

Common - Prepare Json Request
    [Arguments]    ${template}
    ${request}=     Copy Dictionary    ${template}
    Set Test Variable    ${request}    ${request}
    Log    ${request}


Common - Http response should be '${http_status}'
    ${status}=    Set Variable    ${response.status_code}
    Log    ${response.text}
    Should Be Equal As Integers    ${status}    ${http_status}


Common - Response Field '${json_path}' Should Be Equal '${expected}'
    ${response.json}=    To Json    ${response.text}
    ${actual}=    Get Value From Json    ${response.json}    $.${json_path}
    Should Be Equal As Strings    ${actual[0]}    ${expected}
